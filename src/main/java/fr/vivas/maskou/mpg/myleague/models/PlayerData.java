package fr.vivas.maskou.mpg.myleague.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PlayerData {

    private double averageRanking;
    private double goalKeeperAverageRating;
    private double defenseAverageRating;
    private double midfielAveragedRating;
    private double attackAveragedRating;
    private double teamAverageRating;

    private double rotaldos;
    private double averageRotaldos;

    private Integer nbBadges;
    private double goals;

    private HashMap<String, Integer> bonus = new HashMap<>();
    private List<Scorer> scorers = new ArrayList<>();

    public PlayerData() {
    }

    public double getGoalKeeperAverageRating() {
        return goalKeeperAverageRating;
    }

    public void setGoalKeeperAverageRating(double goalKeeperAverageRating) {
        this.goalKeeperAverageRating = goalKeeperAverageRating;
    }

    public double getDefenseAverageRating() {
        return defenseAverageRating;
    }

    public void setDefenseAverageRating(double defenseAverageRating) {
        this.defenseAverageRating = defenseAverageRating;
    }

    public double getMidfielAveragedRating() {
        return midfielAveragedRating;
    }

    public void setMidfielAveragedRating(double midfielAveragedRating) {
        this.midfielAveragedRating = midfielAveragedRating;
    }

    public double getAttackAveragedRating() {
        return attackAveragedRating;
    }

    public void setAttackAveragedRating(double attackAveragedRating) {
        this.attackAveragedRating = attackAveragedRating;
    }

    public double getTeamAverageRating() {
        return teamAverageRating;
    }

    public void setTeamAverageRating(double teamAverageRating) {
        this.teamAverageRating = teamAverageRating;
    }

    public double getAverageRotaldos() {
        return averageRotaldos;
    }

    public void setAverageRotaldos(double averageRotaldos) {
        this.averageRotaldos = averageRotaldos;
    }

    public double getRotaldos() {
        return rotaldos;
    }

    public void setRotaldos(double rotaldos) {
        this.rotaldos = rotaldos;
    }

    public double getAverageRanking() {
        return averageRanking;
    }

    public void setAverageRanking(double averageRanking) {
        this.averageRanking = averageRanking;
    }

    public Integer getNbBadges() {
        return nbBadges;
    }

    public void setNbBadges(Integer nbBadges) {
        this.nbBadges = nbBadges;
    }

    public double getGoals() {
        return goals;
    }

    public void setGoals(double goals) {
        this.goals = goals;
    }

    public HashMap<String, Integer> getBonus() {
        return bonus;
    }

    public void setBonus(HashMap<String, Integer> bonus) {
        this.bonus = bonus;
    }

    public List<Scorer> getScorers() {
        return scorers;
    }

    public void setScorers(List<Scorer> scorers) {
        this.scorers = scorers;
    }
}
