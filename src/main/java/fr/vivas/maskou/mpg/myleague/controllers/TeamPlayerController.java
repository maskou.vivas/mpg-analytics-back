package fr.vivas.maskou.mpg.myleague.controllers;

import fr.vivas.maskou.mpg.myleague.models.TeamPlayer;
import fr.vivas.maskou.mpg.myleague.models.User;
import fr.vivas.maskou.mpg.myleague.mpg.out.Match;
import fr.vivas.maskou.mpg.myleague.services.LeagueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping(path = "/players")
public class TeamPlayerController {

    @Autowired
    private LeagueService leagueService;

    @CrossOrigin(origins="http://localhost:4200")
    @RequestMapping(value = "/{leagueid}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<TeamPlayer> index(@PathVariable("leagueid") String ligueId,
                                  @RequestBody User user,
                                  HttpSession session) {
        //leagueService.initLeagueResults(ligueId, user, session);

        return leagueService.getLeaguePlayers(ligueId, user, session);
    }

}
