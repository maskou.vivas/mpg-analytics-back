package fr.vivas.maskou.mpg.myleague.mpg.out;

import fr.vivas.maskou.mpg.myleague.models.League;

import java.util.List;

public class DataLeaguesOut {

    private List<League> leagues;

    public DataLeaguesOut() {
    }

    public List<League> getLeagues() {
        return leagues;
    }

    public void setLeagues(List<League> leagues) {
        this.leagues = leagues;
    }

    @Override
    public String toString() {
        return "{" +
                "leagues: " + leagues +
                '}';
    }
}
