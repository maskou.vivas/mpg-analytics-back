package fr.vivas.maskou.mpg.myleague.mpg.out;

import java.util.List;

public class Players {

    private List<PlayerResult> home;
    private List<PlayerResult> away;

    public Players() {
    }

    public List<PlayerResult> getHome() {
        return home;
    }

    public void setHome(List<PlayerResult> home) {
        this.home = home;
    }

    public List<PlayerResult> getAway() {
        return away;
    }

    public void setAway(List<PlayerResult> away) {
        this.away = away;
    }

    @Override
    public String toString() {
        return "{" +
                "home: " + home +
                ", away: " + away +
                '}';
    }
}
