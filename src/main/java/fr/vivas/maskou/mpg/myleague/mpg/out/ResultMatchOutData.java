package fr.vivas.maskou.mpg.myleague.mpg.out;

public class ResultMatchOutData {

    private Bonus bonus;
    private String dateMatch;
    private Players players;
    private String rating;
    private String stadium;
    private ResultMatchTeam teamAway;
    private ResultMatchTeam teamHome;

    public ResultMatchOutData() {
    }

    public Bonus getBonus() {
        return bonus;
    }

    public void setBonus(Bonus bonus) {
        this.bonus = bonus;
    }

    public String getDateMatch() {
        return dateMatch;
    }

    public void setDateMatch(String dateMatch) {
        this.dateMatch = dateMatch;
    }

    public Players getPlayers() {
        return players;
    }

    public void setPlayers(Players players) {
        this.players = players;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getStadium() {
        return stadium;
    }

    public void setStadium(String stadium) {
        this.stadium = stadium;
    }

    public ResultMatchTeam getTeamAway() {
        return teamAway;
    }

    public void setTeamAway(ResultMatchTeam teamAway) {
        this.teamAway = teamAway;
    }

    public ResultMatchTeam getTeamHome() {
        return teamHome;
    }

    public void setTeamHome(ResultMatchTeam teamHome) {
        this.teamHome = teamHome;
    }

    @Override
    public String toString() {
        return "{" +
                "bonus: " + bonus +
                ", dateMatch: '" + dateMatch + '\'' +
                ", players: " + players +
                ", rating: '" + rating + '\'' +
                ", stadium: '" + stadium + '\'' +
                ", teamAway: " + teamAway +
                ", teamHome: " + teamHome +
                '}';
    }
}
