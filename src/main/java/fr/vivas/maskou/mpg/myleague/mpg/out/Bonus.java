package fr.vivas.maskou.mpg.myleague.mpg.out;

public class Bonus {

    private BonusDetail home;
    private BonusDetail away;

    public Bonus() {
    }

    public BonusDetail getHome() {
        return home;
    }

    public void setHome(BonusDetail home) {
        this.home = home;
    }

    public BonusDetail getAway() {
        return away;
    }

    public void setAway(BonusDetail away) {
        this.away = away;
    }

    @Override
    public String toString() {
        return "{" +
                "home: " + home +
                ", away: " + away +
                '}';
    }
}
