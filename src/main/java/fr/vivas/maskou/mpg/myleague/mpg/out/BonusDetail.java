package fr.vivas.maskou.mpg.myleague.mpg.out;

public class BonusDetail {

    private String playerName;
    private Integer type;

    public BonusDetail() {
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "{" +
                "playerName: '" + playerName + '\'' +
                ", type: " + type +
                '}';
    }
}
