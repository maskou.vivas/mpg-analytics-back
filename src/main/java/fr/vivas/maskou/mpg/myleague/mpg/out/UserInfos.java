package fr.vivas.maskou.mpg.myleague.mpg.out;

import java.util.Date;
import java.util.List;

public class UserInfos {

    private String id;
    private String email;
    private String firstname;
    private String lastname;
    private String country;
    private Integer teamId;
    private Date dob;
    private Boolean onboarded;
    private Integer push_wall;
    private Integer optin_nl;
    private Integer optin_partner;
    private List<String> jerseySkin;
    private Integer money;
    private Integer bounce;
    private Boolean hasBeenExpert;
    private Integer optinNlMpp;
    private Integer pushNlMpp;
    private Date liveRating;

    public UserInfos() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getTeamId() {
        return teamId;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Boolean getOnboarded() {
        return onboarded;
    }

    public void setOnboarded(Boolean onboarded) {
        this.onboarded = onboarded;
    }

    public Integer getPush_wall() {
        return push_wall;
    }

    public void setPush_wall(Integer push_wall) {
        this.push_wall = push_wall;
    }

    public Integer getOptin_nl() {
        return optin_nl;
    }

    public void setOptin_nl(Integer optin_nl) {
        this.optin_nl = optin_nl;
    }

    public Integer getOptin_partner() {
        return optin_partner;
    }

    public void setOptin_partner(Integer optin_partner) {
        this.optin_partner = optin_partner;
    }

    public List<String> getJerseySkin() {
        return jerseySkin;
    }

    public void setJerseySkin(List<String> jerseySkin) {
        this.jerseySkin = jerseySkin;
    }

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }

    public Integer getBounce() {
        return bounce;
    }

    public void setBounce(Integer bounce) {
        this.bounce = bounce;
    }

    public Boolean getHasBeenExpert() {
        return hasBeenExpert;
    }

    public void setHasBeenExpert(Boolean hasBeenExpert) {
        this.hasBeenExpert = hasBeenExpert;
    }

    public Integer getOptinNlMpp() {
        return optinNlMpp;
    }

    public void setOptinNlMpp(Integer optinNlMpp) {
        this.optinNlMpp = optinNlMpp;
    }

    public Integer getPushNlMpp() {
        return pushNlMpp;
    }

    public void setPushNlMpp(Integer pushNlMpp) {
        this.pushNlMpp = pushNlMpp;
    }

    public Date getLiveRating() {
        return liveRating;
    }

    public void setLiveRating(Date liveRating) {
        this.liveRating = liveRating;
    }

    @Override
    public String toString() {
        return "{" +
                "id:'" + id + '\'' +
                ", email:'" + email + '\'' +
                ", firstname:'" + firstname + '\'' +
                ", lastname:'" + lastname + '\'' +
                ", country:'" + country + '\'' +
                ", teamId:" + teamId +
                ", dob:" + dob +
                ", onboarded:" + onboarded +
                ", push_wall:" + push_wall +
                ", optin_nl:" + optin_nl +
                ", optin_partner:" + optin_partner +
                ", jerseySkin:" + jerseySkin +
                ", money:" + money +
                ", bounce:" + bounce +
                ", hasBeenExpert:" + hasBeenExpert +
                ", optinNlMpp:" + optinNlMpp +
                ", pushNlMpp:" + pushNlMpp +
                ", liveRating:" + liveRating +
                '}';
    }
}
