package fr.vivas.maskou.mpg.myleague.models;

public class League {

    private String id;
    private String admin_mpg_user_id;
    private String current_mpg_user;
    private String name;
    private String url;
    private Integer leagueStatus;
    private Integer championship;
    private Integer mode;
    private Integer teamStatus;
    private Integer players;

    public League() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAdmin_mpg_user_id() {
        return admin_mpg_user_id;
    }

    public void setAdmin_mpg_user_id(String admin_mpg_user_id) {
        this.admin_mpg_user_id = admin_mpg_user_id;
    }

    public String getCurrent_mpg_user() {
        return current_mpg_user;
    }

    public void setCurrent_mpg_user(String current_mpg_user) {
        this.current_mpg_user = current_mpg_user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getLeagueStatus() {
        return leagueStatus;
    }

    public void setLeagueStatus(Integer leagueStatus) {
        this.leagueStatus = leagueStatus;
    }

    public Integer getChampionship() {
        return championship;
    }

    public void setChampionship(Integer championship) {
        this.championship = championship;
    }

    public Integer getMode() {
        return mode;
    }

    public void setMode(Integer mode) {
        this.mode = mode;
    }

    public Integer getTeamStatus() {
        return teamStatus;
    }

    public void setTeamStatus(Integer teamStatus) {
        this.teamStatus = teamStatus;
    }

    public Integer getPlayers() {
        return players;
    }

    public void setPlayers(Integer players) {
        this.players = players;
    }

    @Override
    public String toString() {
        return "{" +
                "id:" + id +
                ", admin_mpg_user_id: '" + admin_mpg_user_id + '\'' +
                ", current_mpg_user :'" + current_mpg_user + '\'' +
                ", name :'" + name + '\'' +
                ", url: '" + url + '\'' +
                ", leagueStatus: " + leagueStatus +
                ", championship: " + championship +
                ", mode: " + mode +
                ", teamStatus: " + teamStatus +
                ", players: " + players +
                '}';
    }
}
