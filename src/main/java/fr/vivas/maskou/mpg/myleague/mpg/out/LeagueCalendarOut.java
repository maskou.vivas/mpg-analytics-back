package fr.vivas.maskou.mpg.myleague.mpg.out;

import fr.vivas.maskou.mpg.myleague.models.League;

import java.util.List;

public class LeagueCalendarOut {

   private DataCalendarOut data;

    public LeagueCalendarOut() {


    }

    public DataCalendarOut getData() {
        return data;
    }

    public void setData(DataCalendarOut data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "{" +
                "data: " + data +
                '}';
    }
}
