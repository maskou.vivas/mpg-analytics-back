package fr.vivas.maskou.mpg.myleague.mpg.out;

public class ResultTeam {

    private String id;
    private String level;
    private String name;
    private String score;
    private String targetMan;
    private String user;

    public ResultTeam() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getTargetMan() {
        return targetMan;
    }

    public void setTargetMan(String targetMan) {
        this.targetMan = targetMan;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "{" +
                "id: '" + id + '\'' +
                ", level: '" + level + '\'' +
                ", name: '" + name + '\'' +
                ", score: '" + score + '\'' +
                ", targetMan: '" + targetMan + '\'' +
                ", user: '" + user + '\'' +
                '}';
    }
}
