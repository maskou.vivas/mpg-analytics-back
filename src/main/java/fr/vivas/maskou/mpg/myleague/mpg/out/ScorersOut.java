package fr.vivas.maskou.mpg.myleague.mpg.out;

import java.util.List;

public class ScorersOut {

    private List<PlayerScorer> scorer;

    public ScorersOut() {
    }

    public List<PlayerScorer> getScorer() {
        return scorer;
    }

    public void setScorer(List<PlayerScorer> scorer) {
        this.scorer = scorer;
    }

    @Override
    public String toString() {
        return "{" +
                "scorer:" + scorer +
                '}';
    }
}
