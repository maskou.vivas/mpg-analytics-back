package fr.vivas.maskou.mpg.myleague.models;

import java.util.ArrayList;
import java.util.List;

public class TeamRankingHistoryResponse {

    private List<TeamRankingDataSet> dataSets;
    private List<String> chartLabels;


    public TeamRankingHistoryResponse() {
        this.dataSets = new ArrayList<>();
        this.chartLabels = new ArrayList<>();
    }

    public List<TeamRankingDataSet> getDataSets() {
        return dataSets;
    }

    public void setDataSets(List<TeamRankingDataSet> dataSets) {
        this.dataSets = dataSets;
    }

    public List<String> getChartLabels() {
        return chartLabels;
    }

    public void setChartLabels(List<String> chartLabels) {
        this.chartLabels = chartLabels;
    }
}
