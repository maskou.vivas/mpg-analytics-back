package fr.vivas.maskou.mpg.myleague.models;

public class User {
    private String login;
    private String token;
    private String userId;


    public User() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "{" +
                "login: '" + login + '\'' +
                ", token: '" + token + '\'' +
                ", userId: '" + userId + '\'' +
                '}';
    }
}
