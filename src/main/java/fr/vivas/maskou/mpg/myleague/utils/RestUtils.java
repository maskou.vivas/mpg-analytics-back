package fr.vivas.maskou.mpg.myleague.utils;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.util.Arrays;

public class RestUtils {

    public static HttpHeaders getHttpHeaders(String token) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("client-version", "5.8.0");
        if (token != null) {
            headers.set("Authorization", token);
        }
        return headers;
    }
}
