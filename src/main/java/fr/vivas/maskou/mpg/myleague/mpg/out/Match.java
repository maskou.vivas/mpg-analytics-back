package fr.vivas.maskou.mpg.myleague.mpg.out;

public class Match {

    private String id;
    private ResultTeam teamAway;
    private ResultTeam teamHome;

    public Match() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ResultTeam getTeamAway() {
        return teamAway;
    }

    public void setTeamAway(ResultTeam teamAway) {
        this.teamAway = teamAway;
    }

    public ResultTeam getTeamHome() {
        return teamHome;
    }

    public void setTeamHome(ResultTeam teamHome) {
        this.teamHome = teamHome;
    }

    @Override
    public String toString() {
        return "{" +
                "id: '" + id + '\'' +
                ", teamAway: " + teamAway +
                ", teamHome: " + teamHome +
                '}';
    }
}
