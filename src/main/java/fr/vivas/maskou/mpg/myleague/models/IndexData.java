package fr.vivas.maskou.mpg.myleague.models;

public class IndexData {

    private String teamName;
    private PlayerData player;
    private PlayerData opponent;

    public IndexData() {
    }

    public PlayerData getOpponent() {
        return opponent;
    }

    public void setOpponent(PlayerData opponent) {
        this.opponent = opponent;
    }

    public PlayerData getPlayer() {
        return player;
    }

    public void setPlayer(PlayerData player) {
        this.player = player;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }
}
