package fr.vivas.maskou.mpg.myleague.models;

public class Scorer {
    private String player;
    private Integer real;
    private Integer mpg;
    private Integer goals;

    public Scorer() {
    }

    public Scorer(String player, Integer real, Integer mpg, Integer goals) {
        this.player = player;
        this.real = real;
        this.mpg = mpg;
        this.goals = goals;
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public Integer getReal() {
        return real;
    }

    public void setReal(Integer real) {
        this.real = real;
    }

    public Integer getMpg() {
        return mpg;
    }

    public void setMpg(Integer mpg) {
        this.mpg = mpg;
    }

    public Integer getGoals() {
        return goals;
    }

    public void setGoals(Integer goals) {
        this.goals = goals;
    }

    @Override
    public String toString() {
        return "{" +
                "player:'" + player + '\'' +
                ", real:" + real +
                ", mpg:" + mpg +
                ", goals:" + goals +
                '}';
    }
}
