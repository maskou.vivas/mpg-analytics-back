package fr.vivas.maskou.mpg.myleague.mpg.out;

public class RankingData {

    private Boolean bonusUser;
    private Integer defeat;
    private Integer difference;
    private Integer draw;
    private Integer goal;
    private Integer goalconceded;
    private Integer played;
    private Integer points;
    private Integer rank;
    private Boolean rotaldo;
    private String series;
    private Boolean targetMan;
    private String teamid;
    private Integer variation;
    private Integer victory;

    public RankingData() {
    }

    public Boolean getBonusUser() {
        return bonusUser;
    }

    public void setBonusUser(Boolean bonusUser) {
        this.bonusUser = bonusUser;
    }

    public Integer getDefeat() {
        return defeat;
    }

    public void setDefeat(Integer defeat) {
        this.defeat = defeat;
    }

    public Integer getDifference() {
        return difference;
    }

    public void setDifference(Integer difference) {
        this.difference = difference;
    }

    public Integer getDraw() {
        return draw;
    }

    public void setDraw(Integer draw) {
        this.draw = draw;
    }

    public Integer getGoal() {
        return goal;
    }

    public void setGoal(Integer goal) {
        this.goal = goal;
    }

    public Integer getGoalconceded() {
        return goalconceded;
    }

    public void setGoalconceded(Integer goalconceded) {
        this.goalconceded = goalconceded;
    }

    public Integer getPlayed() {
        return played;
    }

    public void setPlayed(Integer played) {
        this.played = played;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Boolean getRotaldo() {
        return rotaldo;
    }

    public void setRotaldo(Boolean rotaldo) {
        this.rotaldo = rotaldo;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public Boolean getTargetMan() {
        return targetMan;
    }

    public void setTargetMan(Boolean targetMan) {
        this.targetMan = targetMan;
    }

    public String getTeamid() {
        return teamid;
    }

    public void setTeamid(String teamid) {
        this.teamid = teamid;
    }

    public Integer getVariation() {
        return variation;
    }

    public void setVariation(Integer variation) {
        this.variation = variation;
    }

    public Integer getVictory() {
        return victory;
    }

    public void setVictory(Integer victory) {
        this.victory = victory;
    }


}
