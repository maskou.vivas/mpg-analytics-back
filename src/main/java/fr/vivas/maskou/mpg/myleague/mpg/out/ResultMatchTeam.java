package fr.vivas.maskou.mpg.myleague.mpg.out;

import java.util.List;

public class ResultMatchTeam {

    private String abbr;
    private List<String> badges;
    private String coach;
    private String composition;
    private String id;
    private String mpguser;
    private String name;
    private String ranking;
    private Integer score;
    private Boolean star;
    private List<SubstituteResult> substitutes;
    private String userId;

    public ResultMatchTeam() {
    }

    public String getAbbr() {
        return abbr;
    }

    public void setAbbr(String abbr) {
        this.abbr = abbr;
    }

    public List<String> getBadges() {
        return badges;
    }

    public void setBadges(List<String> badges) {
        this.badges = badges;
    }

    public String getCoach() {
        return coach;
    }

    public void setCoach(String coach) {
        this.coach = coach;
    }

    public String getComposition() {
        return composition;
    }

    public void setComposition(String composition) {
        this.composition = composition;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMpguser() {
        return mpguser;
    }

    public void setMpguser(String mpguser) {
        this.mpguser = mpguser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRanking() {
        return ranking;
    }

    public void setRanking(String ranking) {
        this.ranking = ranking;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Boolean getStar() {
        return star;
    }

    public void setStar(Boolean star) {
        this.star = star;
    }

    public List<SubstituteResult> getSubstitutes() {
        return substitutes;
    }

    public void setSubstitutes(List<SubstituteResult> substitutes) {
        this.substitutes = substitutes;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "{" +
                "abbr: '" + abbr + '\'' +
                ", badges: " + badges +
                ", coach: '" + coach + '\'' +
                ", composition: '" + composition + '\'' +
                ", id: '" + id + '\'' +
                ", mpguser: '" + mpguser + '\'' +
                ", name: '" + name + '\'' +
                ", ranking: " + ranking +
                ", score: " + score +
                ", star: " + star +
                ", substitutes: " + substitutes +
                ", userId: '" + userId + '\'' +
                '}';
    }
}
