package fr.vivas.maskou.mpg.myleague.controllers;


import fr.vivas.maskou.mpg.myleague.models.User;
import fr.vivas.maskou.mpg.myleague.mpg.out.ResultMatchOutData;
import fr.vivas.maskou.mpg.myleague.services.MatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping(path = "/match")
public class MatchController {

    @Autowired
    private MatchService matchService;

    @RequestMapping(value = "/{leagueid}/{matchid}",
            method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResultMatchOutData getMatch(@PathVariable("leagueid") String leagueId,
                                       @PathVariable("matchid") String matchId,
                                       HttpSession session) {
        User user = (User) session.getAttribute("user");
        return matchService.getMatch(matchId,leagueId, user);
    }

}
