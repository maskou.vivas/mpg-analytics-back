package fr.vivas.maskou.mpg.myleague.services;

import fr.vivas.maskou.mpg.myleague.models.*;
import fr.vivas.maskou.mpg.myleague.mpg.out.*;
import fr.vivas.maskou.mpg.myleague.utils.RestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class LeagueScorerService {
    private static final Logger log = LoggerFactory.getLogger(LeagueScorerService.class);

    private static final String URL_LEAGUE_SCORER = "https://api.monpetitgazon.com/league/<ligue_id>/ranking/scorer";


    public List<PlayerScorer> getLeagueScorers (String ligueId, User user) {

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = RestUtils.getHttpHeaders(user.getToken());
        HttpEntity<LeaguesOut> entity = new HttpEntity<LeaguesOut>(headers);
        String urlScorer = URL_LEAGUE_SCORER.replace("<ligue_id>", ligueId);

        ResponseEntity<ScorersOut> response = restTemplate.exchange(urlScorer,HttpMethod.GET,
                entity, ScorersOut.class);

        if (response.getStatusCode() == HttpStatus.OK) {
            return response.getBody().getScorer();
        }
        return null;
    }

    public List<Scorer> getLeagueTeamScorers(String ligueId, User user) {
        List<PlayerScorer> leagueScorers = getLeagueScorers(ligueId, user);
        List<Scorer> teamScorers = leagueScorers.stream()
                .filter(scorer -> scorer.getTeamid().equals("mpg_team_"+ ligueId + "$$" + user.getUserId()))
                .map(scorer -> new Scorer(scorer.getPlayer(), scorer.getReal(), scorer.getMpg(), scorer.getGoals()))
                .collect(Collectors.toList());

        return teamScorers;

    }

}
