package fr.vivas.maskou.mpg.myleague.mpg.out;

public class ResultMatchOut {

    private ResultMatchOutData data;

    public ResultMatchOut() {
    }

    public ResultMatchOutData getData() {
        return data;
    }

    public void setData(ResultMatchOutData data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "{" +
                "data: " + data +
                '}';
    }
}
