package fr.vivas.maskou.mpg.myleague.mpg.out;

public class MpgConnectionOut {
    private String action;
    private String createdAt;
    private String language;
    private Boolean onboarded;
    //eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6Im1wZ191c2VyXzE1MTQ2NzkiLCJjaGVjayI6ImY0M2QzMjA5NDAxYWE3MzciLCJpYXQiOjE1NzQ3NjE4MDV9.QwV9QxbEen7axVq4tmDScnFml_vDMahzhKZLVV6RojY
    private String token;
    private String userId; // "mpg_user_1514679"

    public MpgConnectionOut() {
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Boolean getOnboarded() {
        return onboarded;
    }

    public void setOnboarded(Boolean onboarded) {
        this.onboarded = onboarded;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
