package fr.vivas.maskou.mpg.myleague.services;

import fr.vivas.maskou.mpg.myleague.models.League;
import fr.vivas.maskou.mpg.myleague.models.User;
import fr.vivas.maskou.mpg.myleague.models.UserIn;
import fr.vivas.maskou.mpg.myleague.mpg.out.LeaguesOut;
import fr.vivas.maskou.mpg.myleague.mpg.out.MpgConnectionIn;
import fr.vivas.maskou.mpg.myleague.mpg.out.MpgConnectionOut;
import fr.vivas.maskou.mpg.myleague.mpg.out.UserInfos;
import fr.vivas.maskou.mpg.myleague.services.dto.LeagueWithPlayers;
import fr.vivas.maskou.mpg.myleague.utils.RestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ConnectionService {
    private static final Logger log = LoggerFactory.getLogger(ConnectionService.class);

    private static final String URl_SIGN_IN = "https://api.monpetitgazon.com/user/signIn";
    private static final String URl_USER_INFO = "https://api.monpetitgazon.com/user";


    public User connect(UserIn user) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = RestUtils.getHttpHeaders(null);
        MpgConnectionIn mpgConnectionIn = new MpgConnectionIn();
        mpgConnectionIn.setEmail(user.getLogin());
        mpgConnectionIn.setPassword(user.getPassword());
        mpgConnectionIn.setLanguage("fr-FR");

        HttpEntity<MpgConnectionIn> entity = new HttpEntity<MpgConnectionIn>(mpgConnectionIn, headers);

        ResponseEntity<MpgConnectionOut> response = restTemplate.exchange(URl_SIGN_IN, HttpMethod.POST,
                entity, MpgConnectionOut.class);

        User userResponse = new User();
        userResponse.setLogin(user.getLogin());
        userResponse.setToken(response.getBody().getToken());
        userResponse.setUserId(response.getBody().getUserId());

        return userResponse;
    }

    public UserInfos getUserInfo(User user) {

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = RestUtils.getHttpHeaders(user.getToken());
        HttpEntity<UserInfos> entity = new HttpEntity<>(headers);

        ResponseEntity<UserInfos> response = restTemplate.exchange(URl_USER_INFO,HttpMethod.GET,
                entity, UserInfos.class);

        if (response.getStatusCode() == HttpStatus.OK) {
            return response.getBody();
        }
        return null;
    }
}
