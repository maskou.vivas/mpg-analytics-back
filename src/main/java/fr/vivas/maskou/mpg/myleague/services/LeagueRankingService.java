package fr.vivas.maskou.mpg.myleague.services;

import fr.vivas.maskou.mpg.myleague.models.User;
import fr.vivas.maskou.mpg.myleague.mpg.out.*;
import fr.vivas.maskou.mpg.myleague.services.dto.TeamRankingData;
import fr.vivas.maskou.mpg.myleague.utils.RestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpSession;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class LeagueRankingService {
    private static final Logger log = LoggerFactory.getLogger(LeagueRankingService.class);
    private static final String URL_RANKING_LEAGUE = "https://api.monpetitgazon.com/league/<league_id>/ranking";

    @Autowired
    private LeagueService leagueService;

    public LeagueRanking getRanking(String leagueId, User user) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = RestUtils.getHttpHeaders(user.getToken());
        HttpEntity<LeaguesOut> entity = new HttpEntity<LeaguesOut>(headers);
        String urlGetMatch = URL_RANKING_LEAGUE.replace("<league_id>", leagueId);
        try {
            ResponseEntity<LeagueRanking> response = restTemplate.exchange(urlGetMatch, HttpMethod.GET,
                    entity, LeagueRanking.class);
            if (response.getStatusCode() == HttpStatus.OK) {
                return response.getBody();
            }
            return null;
        } catch (HttpClientErrorException e) {
            log.info("Impossible de récuperer le classement de la ligue: " + leagueId);
            return null;
        }
    }

    /**
     * Retourne un historique des classements de la league par journée.
     * @param ligueId Id de la ligue
     * @return Liste des classements
     */
    public LinkedHashMap<Integer, LinkedHashMap<String, TeamRankingData>> getLeagueRankingHistory(String ligueId, HttpSession session) {
        List<Results> leagueResults = (List<Results>) session.getAttribute("leagueResults_" + ligueId);
        Map<String, TeamRankingData> pointsTeamMap = new HashMap<>();
        LinkedHashMap<Integer, LinkedHashMap<String, TeamRankingData>> dayRankingList = new LinkedHashMap<>();

        for (Results results: leagueResults) {
            LinkedHashMap<String, TeamRankingData> dayRanking = getDayRanking(pointsTeamMap, results);
            dayRankingList.put(results.getCurrentMatchDay(), dayRanking);
        }

        return dayRankingList;
    }

    private LinkedHashMap<String, TeamRankingData> getDayRanking(Map<String, TeamRankingData> pointsTeamMap, Results results) {
        Map<String, TeamRankingData> dayPointsTeamMap = new HashMap<>();
        for (Match match : results.getMatches()) {
            initTeamForRanking(pointsTeamMap, match);
            calculatePointsAndGoalAverageMatch(pointsTeamMap, match, dayPointsTeamMap);
        }

        //calcul du classement de la journée
        Comparator<TeamRankingData> teamRankingDataComparator = Comparator
                .comparing(TeamRankingData::getPoints)
                .thenComparing(TeamRankingData::getGoalAverage);

        LinkedHashMap<String, TeamRankingData> collect = dayPointsTeamMap.entrySet()
                .stream()
                .sorted((e1, e2) -> teamRankingDataComparator.reversed().compare(e1.getValue(), e2.getValue()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

        log.info(collect.toString());
        return collect;
    }

    private void calculatePointsAndGoalAverageMatch(Map<String, TeamRankingData> pointsTeamMap, Match match,
                                                    Map<String, TeamRankingData> dayPointsTeamMap) {
        if (match.getTeamHome().getScore() != null  && match.getTeamAway().getScore() != null) {

            int scoreHome = Integer.parseInt(match.getTeamHome().getScore());
            int scoreAway = Integer.parseInt(match.getTeamAway().getScore());
            if ( scoreHome > scoreAway ) {
                //victoire domicile
                pointsTeamMap.get(match.getTeamHome().getName()).points += 3;
                pointsTeamMap.get(match.getTeamHome().getName()).goalAverage += (scoreHome - scoreAway);

                pointsTeamMap.get(match.getTeamAway().getName()).goalAverage -= (scoreHome - scoreAway);

            } else if (scoreHome < scoreAway) {
                //victoire exterieur
                pointsTeamMap.get(match.getTeamAway().getName()).points += 3;
                pointsTeamMap.get(match.getTeamAway().getName()).goalAverage += (scoreAway - scoreHome);

                pointsTeamMap.get(match.getTeamHome().getName()).goalAverage -= (scoreAway - scoreHome);
            } else {
                //match nul
                pointsTeamMap.get(match.getTeamHome().getName()).points += 1;
                pointsTeamMap.get(match.getTeamAway().getName()).points += 1;
            }
            TeamRankingData homeRankingData = new TeamRankingData();
            TeamRankingData awayRankingData = new TeamRankingData();

            initTeamDayRankingData(pointsTeamMap, match.getTeamHome().getName(), homeRankingData);
            initTeamDayRankingData(pointsTeamMap, match.getTeamAway().getName(), awayRankingData);

            dayPointsTeamMap.put(match.getTeamHome().getName(), homeRankingData);
            dayPointsTeamMap.put(match.getTeamAway().getName(), awayRankingData);
        }


    }

    private void initTeamDayRankingData(Map<String, TeamRankingData> pointsTeamMap, String teamName, TeamRankingData homeRankingData) {
        homeRankingData.points = pointsTeamMap.get(teamName).points;
        homeRankingData.goalAverage = pointsTeamMap.get(teamName).goalAverage;
    }

    private void initTeamForRanking(Map<String, TeamRankingData> pointsTeamMap, Match match) {
        if (!pointsTeamMap.containsKey(match.getTeamHome().getName())) {
            pointsTeamMap.put(match.getTeamHome().getName(), new TeamRankingData());
        }
        if (!pointsTeamMap.containsKey(match.getTeamAway().getName())) {
            pointsTeamMap.put(match.getTeamAway().getName(), new TeamRankingData());
        }
    }

}
