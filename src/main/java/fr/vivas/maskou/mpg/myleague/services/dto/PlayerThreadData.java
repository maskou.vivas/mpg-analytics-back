package fr.vivas.maskou.mpg.myleague.services.dto;


import java.util.HashMap;

public class PlayerThreadData {
    public double goalKeeperRate = 0;
    public double defenseRating = 0;
    public double midfieldRating = 0;
    public double attackRating = 0;
    public double ranking = 0;
    public double rotaldos = 0;
    public HashMap<String, Integer> bonus = new HashMap<>();
    public String teamName;


    public PlayerThreadData() {
    }
}
