package fr.vivas.maskou.mpg.myleague.models;

public class UserIn {
    private String login;
    private String password;

    public UserIn() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
