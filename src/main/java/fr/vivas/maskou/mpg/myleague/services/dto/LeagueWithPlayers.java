package fr.vivas.maskou.mpg.myleague.services.dto;

import fr.vivas.maskou.mpg.myleague.models.League;
import fr.vivas.maskou.mpg.myleague.models.TeamPlayer;

import java.util.List;

public class LeagueWithPlayers {
    public League leagueInfo;
    public List<TeamPlayer> players;

    public LeagueWithPlayers() {
    }

    public LeagueWithPlayers(League leagueInfo, List<TeamPlayer> players) {
        this.leagueInfo = leagueInfo;
        this.players = players;
    }
}
