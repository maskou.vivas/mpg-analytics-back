package fr.vivas.maskou.mpg.myleague.mpg.out;

public class LeaguesOut {

    private DataLeaguesOut data;

    public LeaguesOut() {
    }

    public DataLeaguesOut getData() {
        return data;
    }

    public void setData(DataLeaguesOut data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "{" +
                "data: " + data +
                '}';
    }
}
