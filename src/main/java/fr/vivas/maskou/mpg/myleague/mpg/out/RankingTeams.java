package fr.vivas.maskou.mpg.myleague.mpg.out;

public class RankingTeams {
    private String teamid;
    private Boolean rotaldo;
    private Boolean bonusUser;
    private String series;
    private Integer played;
    private Integer victory;
    private Integer draw;
    private Integer defeat;
    private Integer goal;
    private Integer goalconceded;
    private Integer difference;
    private Integer points;
    private Integer rank;
    private Integer variation;

    public RankingTeams() {
    }

    public String getTeamid() {
        return teamid;
    }

    public void setTeamid(String teamid) {
        this.teamid = teamid;
    }

    public Boolean getRotaldo() {
        return rotaldo;
    }

    public void setRotaldo(Boolean rotaldo) {
        this.rotaldo = rotaldo;
    }

    public Boolean getBonusUser() {
        return bonusUser;
    }

    public void setBonusUser(Boolean bonusUser) {
        this.bonusUser = bonusUser;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public Integer getPlayed() {
        return played;
    }

    public void setPlayed(Integer played) {
        this.played = played;
    }

    public Integer getVictory() {
        return victory;
    }

    public void setVictory(Integer victory) {
        this.victory = victory;
    }

    public Integer getDraw() {
        return draw;
    }

    public void setDraw(Integer draw) {
        this.draw = draw;
    }

    public Integer getDefeat() {
        return defeat;
    }

    public void setDefeat(Integer defeat) {
        this.defeat = defeat;
    }

    public Integer getGoal() {
        return goal;
    }

    public void setGoal(Integer goal) {
        this.goal = goal;
    }

    public Integer getGoalconceded() {
        return goalconceded;
    }

    public void setGoalconceded(Integer goalconceded) {
        this.goalconceded = goalconceded;
    }

    public Integer getDifference() {
        return difference;
    }

    public void setDifference(Integer difference) {
        this.difference = difference;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Integer getVariation() {
        return variation;
    }

    public void setVariation(Integer variation) {
        this.variation = variation;
    }

    @Override
    public String toString() {
        return "{" +
                "teamid:'" + teamid + '\'' +
                ", rotaldo:" + rotaldo +
                ", bonusUser:" + bonusUser +
                ", series:'" + series + '\'' +
                ", played:" + played +
                ", victory:" + victory +
                ", draw:" + draw +
                ", defeat:" + defeat +
                ", goal:" + goal +
                ", goalconceded:" + goalconceded +
                ", difference:" + difference +
                ", points:" + points +
                ", rank:" + rank +
                ", variation:" + variation +
                '}';
    }
}
