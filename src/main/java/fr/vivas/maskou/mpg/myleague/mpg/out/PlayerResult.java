package fr.vivas.maskou.mpg.myleague.mpg.out;

public class PlayerResult {
    private Integer bonus;
    private String id;
    private String name;
    private Integer number;
    private Integer position;
    private Double rating;
    private String teamid;
    private SubstitutePlayerResult substitute;

    public PlayerResult() {
    }

    public Integer getBonus() {
        return bonus;
    }

    public void setBonus(Integer bonus) {
        this.bonus = bonus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public String getTeamid() {
        return teamid;
    }

    public void setTeamid(String teamid) {
        this.teamid = teamid;
    }

    public SubstitutePlayerResult getSubstitute() {
        return substitute;
    }

    public void setSubstitute(SubstitutePlayerResult substitute) {
        this.substitute = substitute;
    }

    @Override
    public String toString() {
        return "{" +
                "bonus: " + bonus +
                ", id: '" + id + '\'' +
                ", name: '" + name + '\'' +
                ", number: " + number +
                ", position: " + position +
                ", rating: " + rating +
                ", substitute: " + substitute +
                ", teamid: '" + teamid + '\'' +
                '}';
    }
}
