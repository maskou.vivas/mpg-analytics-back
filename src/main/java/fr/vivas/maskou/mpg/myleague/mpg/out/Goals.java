package fr.vivas.maskou.mpg.myleague.mpg.out;

public class Goals {
    private Integer goal;
    private Integer mpg;
    private Integer own_goal;
    private Integer cancel_goal;
    private Integer cancel_keeper_goal;

    public Goals() {
    }

    public Integer getGoal() {
        return goal;
    }

    public void setGoal(Integer goal) {
        this.goal = goal;
    }

    public Integer getMpg() {
        return mpg;
    }

    public void setMpg(Integer mpg) {
        this.mpg = mpg;
    }

    public Integer getOwn_goal() {
        return own_goal;
    }

    public void setOwn_goal(Integer own_goal) {
        this.own_goal = own_goal;
    }

    public Integer getCancel_goal() {
        return cancel_goal;
    }

    public void setCancel_goal(Integer cancel_goal) {
        this.cancel_goal = cancel_goal;
    }

    public Integer getCancel_keeper_goal() {
        return cancel_keeper_goal;
    }

    public void setCancel_keeper_goal(Integer cancel_keeper_goal) {
        this.cancel_keeper_goal = cancel_keeper_goal;
    }
}
