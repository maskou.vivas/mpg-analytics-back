package fr.vivas.maskou.mpg.myleague.services;

import fr.vivas.maskou.mpg.myleague.models.*;
import fr.vivas.maskou.mpg.myleague.mpg.out.*;
import fr.vivas.maskou.mpg.myleague.mpg.out.Match;
import fr.vivas.maskou.mpg.myleague.services.dto.LeagueWithPlayers;
import fr.vivas.maskou.mpg.myleague.services.dto.PlayerThreadData;
import fr.vivas.maskou.mpg.myleague.services.dto.TeamRankingData;
import fr.vivas.maskou.mpg.myleague.utils.RestUtils;
import org.decimal4j.util.DoubleRounder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpSession;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class LeagueService {
    private static final Logger log = LoggerFactory.getLogger(LeagueService.class);

    private static final String URL_DASHBOARD = "https://api.monpetitgazon.com/user/dashboard";
    private static final String URL_LEAGUE_CALENDAR = "https://api.monpetitgazon.com/league/<ligue_id>/calendar";

    @Autowired
    private MatchService matchService;
    @Autowired
    private LeagueScorerService leagueScorerService;

    public List<LeagueWithPlayers> getLeagues (User user, HttpSession session) {

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = RestUtils.getHttpHeaders(user.getToken());
        HttpEntity<LeaguesOut> entity = new HttpEntity<LeaguesOut>(headers);

        ResponseEntity<LeaguesOut> response = restTemplate.exchange(URL_DASHBOARD,HttpMethod.GET,
                entity, LeaguesOut.class);

        if (response.getStatusCode() == HttpStatus.OK) {
            List<League> leagues = response.getBody().getData().getLeagues();
            return leagues.stream().map(league -> new LeagueWithPlayers(league, getLeaguePlayers(league.getId(), user, session)))
                    .collect(Collectors.toList());
        }
        return null;
    }

    public Results getLeagueCalendar(String ligueId, Integer championshipDay, User user) {

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = RestUtils.getHttpHeaders(user.getToken());
        HttpEntity<LeaguesOut> entity = new HttpEntity<LeaguesOut>(headers);
        String urlCalendar = URL_LEAGUE_CALENDAR.replace("<ligue_id>", ligueId);

        if (championshipDay != null) {
            urlCalendar += "/" + championshipDay;
        }

        ResponseEntity<LeagueCalendarOut> response = restTemplate.exchange(urlCalendar,HttpMethod.GET,
                entity, LeagueCalendarOut.class);

        if (response.getStatusCode() == HttpStatus.OK) {
            return response.getBody().getData().getResults();
        }
        return null;
    }


    /**
     * Calcul de la moyenne de buts marqués par match pour une ligue donnée
     * @param ligueId Id de la ligue
     * @param session Session HTTP
     * @return Retourne la moyenne de buts marqués par match sur une ligue donnée
     */
    public double goalScoredByMatch(String ligueId, User user, HttpSession session) {
        int nbGoal = 0;
        List<Match> matchsUser = (List<Match>) session.getAttribute("userMatchs_league" + ligueId);
        Boolean isCurrentDay = Boolean.FALSE;
        for (Match match : matchsUser) {
            if (match.getTeamAway().getUser().equals(user.getUserId())
                    && !match.getTeamHome().getUser().equals(user.getUserId())) {
                if (match.getTeamAway().getScore() != null) {
                    nbGoal += Integer.parseInt(match.getTeamAway().getScore());
                } else {
                    // score null -> journée en cours
                    isCurrentDay = Boolean.TRUE;
                }
            }
            if (match.getTeamHome().getUser().equals(user.getUserId())
                && !match.getTeamAway().getUser().equals(user.getUserId())) {
                if (match.getTeamHome().getScore() != null) {
                    nbGoal += Integer.parseInt(match.getTeamHome().getScore());
                } else {
                    // score null -> journée en cours
                    isCurrentDay = Boolean.TRUE;
                }
            }
        }

        Integer nbPlayedGames = (Integer) session.getAttribute("league_" + ligueId + "_currentMatchDay");
        if (isCurrentDay && nbPlayedGames != 0) {
           nbPlayedGames -= 1;
        }
        return (double)nbGoal / (double)nbPlayedGames;
    }

    /**
     * Calcul de la moyenne de buts encaissés par match sur une ligue donnée
     * @param ligueId Id de la league
     * @param session Session HTTP
     * @return Retourne la moyenne de buts encaissés par match sur une ligue donnée
     */
    public double goalOwnedByMatch(String ligueId, User user, HttpSession session) {
        int nbGoal = 0;
        List<Match> matchsUser = (List<Match>) session.getAttribute("userMatchs_league" + ligueId);
        Boolean isCurrentDay = Boolean.FALSE;

        for (Match match : matchsUser) {
            if (match.getTeamAway().getUser().equals(user.getUserId())
                    && !match.getTeamHome().getUser().equals(user.getUserId())) {
                if (match.getTeamHome().getScore() != null) {
                    nbGoal += Integer.parseInt(match.getTeamHome().getScore());
                } else {
                    // score null -> journée en cours
                    isCurrentDay = Boolean.TRUE;
                }
            }
            if (match.getTeamHome().getUser().equals(user.getUserId())
                    && !match.getTeamAway().getUser().equals(user.getUserId())) {
                if (match.getTeamAway().getScore() != null) {
                    nbGoal += Integer.parseInt(match.getTeamAway().getScore());
                } else {
                    // score null -> journée en cours
                    isCurrentDay = Boolean.TRUE;
                }
            }
        }

        Integer nbPlayedGames = (Integer) session.getAttribute("league_" + ligueId + "_currentMatchDay");
        if (isCurrentDay && nbPlayedGames != 0) {
            nbPlayedGames -= 1;
        }
        return (double)nbGoal / (double)nbPlayedGames;
    }


    /**
     * Calcul du nombre de badges gagnés sur une ligue donnée
     * @param ligueId Id de la league
     * @param session Session HTTP
     * @return Retourne le nombre de badges gagnés sur une ligue donnée
     */
    public Integer nbBadgesEarned(String ligueId, User user, HttpSession session) {
        int nbBadges = 0;
        List<Match> matchsUser = (List<Match>) session.getAttribute("userMatchs_league" + ligueId);

        for (Match match : matchsUser) {
            ResultMatchOutData matchDetails = matchService.getMatch(match.getId(), ligueId, user);
            if (matchDetails != null) {
                if (match.getTeamAway().getUser().equals(user.getUserId())
                        && !match.getTeamHome().getUser().equals(user.getUserId())) {
                    nbBadges += matchDetails.getTeamAway().getBadges().size();
                }
                if (match.getTeamHome().getUser().equals(user.getUserId())
                        && !match.getTeamAway().getUser().equals(user.getUserId())) {
                    nbBadges += matchDetails.getTeamHome().getBadges().size();
                }
            }
        }

        return nbBadges;
    }

    /**
     * Calcul du nombre de badges gagnés sur une ligue donnée
     * @param ligueId Id de la league
     * @param session Session HTTP
     * @return Retourne le nombre de badges gagnés sur une ligue donnée
     */
    public Integer nbBadgesGiven(String ligueId, User user, HttpSession session) {
        int nbBadges = 0;

        List<Match> matchsUser = (List<Match>) session.getAttribute("userMatchs_league" + ligueId);

        for (Match match : matchsUser) {
            ResultMatchOutData matchDetails = matchService.getMatch(match.getId(), ligueId, user);
            if (matchDetails != null) {
                if (match.getTeamAway().getUser().equals(user.getUserId())
                        && !match.getTeamHome().getUser().equals(user.getUserId())) {
                    nbBadges += matchDetails.getTeamHome().getBadges().size();
                }
                if (match.getTeamHome().getUser().equals(user.getUserId())
                        && !match.getTeamAway().getUser().equals(user.getUserId())) {
                    nbBadges += matchDetails.getTeamAway().getBadges().size();
                }
            }
        }
        return nbBadges;
    }



    public IndexData getPlayerData(String ligueId, User user, HttpSession session) {

        List<Match> matchsUser = (List<Match>) session.getAttribute("userMatchs_league" + ligueId);
        PlayerThreadData opponentThreadData = new PlayerThreadData();
        PlayerThreadData mainPlayerThreadData = new PlayerThreadData();

        Integer nbPlayedGames = 0;
        for (Match match : matchsUser) {
            ResultMatchOutData matchDetails = matchService.getMatch(match.getId(), ligueId, user);
            if (matchDetails != null) {
                if (match.getTeamHome().getUser().equals(user.getUserId())) {
                    mainPlayerThreadData.teamName = matchDetails.getTeamHome().getName();
                    makeCalculStats(mainPlayerThreadData, matchDetails.getPlayers().getHome(), matchDetails.getTeamHome());
                    makeCalculStats(opponentThreadData, matchDetails.getPlayers().getAway(), matchDetails.getTeamAway());
                    if (matchDetails.getBonus() != null) {
                        if (matchDetails.getBonus().getHome() != null) {
                            Integer nbBonus = mainPlayerThreadData.bonus.getOrDefault(getBonusNameFromType(matchDetails.getBonus().getHome().getType()),0);
                            mainPlayerThreadData.bonus.put(getBonusNameFromType(matchDetails.getBonus().getHome().getType()), nbBonus + 1);
                        }
                        if (matchDetails.getBonus().getAway() != null) {
                            Integer nbBonus = opponentThreadData.bonus.getOrDefault(getBonusNameFromType(matchDetails.getBonus().getAway().getType()),0);
                            opponentThreadData.bonus.put(getBonusNameFromType(matchDetails.getBonus().getAway().getType()), nbBonus + 1);
                        }
                    }
                } else {
                    makeCalculStats(mainPlayerThreadData, matchDetails.getPlayers().getAway(), matchDetails.getTeamAway());
                    makeCalculStats(opponentThreadData, matchDetails.getPlayers().getHome(), matchDetails.getTeamHome());
                    if (matchDetails.getBonus() != null) {
                        if (matchDetails.getBonus().getAway() != null) {
                            Integer nbBonus = mainPlayerThreadData.bonus.getOrDefault(getBonusNameFromType(matchDetails.getBonus().getAway().getType()),0);
                            mainPlayerThreadData.bonus.put(getBonusNameFromType(matchDetails.getBonus().getAway().getType()), nbBonus + 1);
                        }
                        if (matchDetails.getBonus().getHome() != null) {
                            Integer nbBonus = opponentThreadData.bonus.getOrDefault(getBonusNameFromType(matchDetails.getBonus().getHome().getType()),0);
                            opponentThreadData.bonus.put(getBonusNameFromType(matchDetails.getBonus().getHome().getType()), nbBonus + 1);
                        }
                    }
                }
                nbPlayedGames += 1;
            }
        }

        PlayerData opponentData = buildPlayerData(opponentThreadData.goalKeeperRate, opponentThreadData.defenseRating,
                opponentThreadData.midfieldRating, opponentThreadData.attackRating, opponentThreadData.ranking,
                opponentThreadData.rotaldos, nbPlayedGames);
        opponentData.setGoals(DoubleRounder.round(goalOwnedByMatch(ligueId, user,session),2));
        opponentData.setNbBadges(nbBadgesGiven(ligueId, user, session));
        opponentData.setBonus(opponentThreadData.bonus);

        PlayerData playerData = buildPlayerData(mainPlayerThreadData.goalKeeperRate, mainPlayerThreadData.defenseRating,
                mainPlayerThreadData.midfieldRating, mainPlayerThreadData.attackRating, mainPlayerThreadData.ranking,
                mainPlayerThreadData.rotaldos, nbPlayedGames);
        playerData.setGoals(DoubleRounder.round(goalScoredByMatch(ligueId, user,session), 2));
        playerData.setNbBadges(nbBadgesEarned(ligueId, user, session));
        playerData.setBonus(mainPlayerThreadData.bonus);
        playerData.setScorers(leagueScorerService.getLeagueTeamScorers(ligueId, user));

        IndexData indexData = new IndexData();
        indexData.setTeamName(mainPlayerThreadData.teamName);
        indexData.setPlayer(playerData);
        indexData.setOpponent(opponentData);

        return indexData;
    }

    private String getBonusNameFromType(Integer type) {
        switch (type) {
            case 1:
                return "valise";
            case 2:
                return "zahia";
            case 3:
                return "suarez";
            case 4:
                return "redbull";
            case 5:
                return "mirroir";
            case 6:
                return "chapron";
            case 7:
                return "tontonpat";
            default:
                return "";
        }
    }

    private void makeCalculStats(PlayerThreadData threadData, List<PlayerResult> players, ResultMatchTeam team) {
        threadData.goalKeeperRate += getFieldPlayerLineAverageRating(players, 1);
        threadData.defenseRating += getFieldPlayerLineAverageRating(players, 2);
        threadData.midfieldRating += getFieldPlayerLineAverageRating(players, 3);
        threadData.attackRating += getFieldPlayerLineAverageRating(players, 4);
        if (team.getRanking().startsWith(">")) {
            threadData.ranking += 100000;
        } else {
            threadData.ranking += Double.parseDouble(team.getRanking());
        }
        threadData.rotaldos += getRotaldos(players);
    }

    private HashMap<String, Integer> getPlayedBonuses() {
        HashMap<String, Integer> bonuses = new HashMap<>();



        return bonuses;
    }

    private PlayerData buildPlayerData(double goalKeeperRate, double defenseRating, double midfieldRating, double attackRating, double ranking, double rotaldos, double nbPlayedGames) {

        PlayerData data = new PlayerData();
        data.setGoalKeeperAverageRating(DoubleRounder.round(goalKeeperRate / nbPlayedGames, 2));
        data.setDefenseAverageRating(DoubleRounder.round(defenseRating / nbPlayedGames,2));
        data.setMidfielAveragedRating(DoubleRounder.round(midfieldRating / nbPlayedGames,2));
        data.setAttackAveragedRating(DoubleRounder.round(attackRating / nbPlayedGames,2));
        data.setAverageRanking(DoubleRounder.round(ranking / nbPlayedGames,0));
        data.setAverageRotaldos(DoubleRounder.round(rotaldos / nbPlayedGames,2));
        data.setRotaldos(DoubleRounder.round(rotaldos, 2));
        data.setTeamAverageRating(
                DoubleRounder.round(
                        (goalKeeperRate + defenseRating + midfieldRating + attackRating) / (nbPlayedGames * 4)
                ,2)
        );
        return data;
    }

    private int getRotaldos(List<PlayerResult> players) {

        return players.stream().filter(playerResult -> playerResult.getSubstitute() != null
                &&  playerResult.getSubstitute().getId().equals("player_0"))
                .collect(Collectors.toList()).size();
    }

    private double getFieldPlayerLineAverageRating(List<PlayerResult> players, int lineType) {
        List<PlayerResult> playerResults = players.stream()
                .filter(player -> player.getPosition() == lineType)
                .collect(Collectors.toList());
        double playerRatings = 0;
        for (PlayerResult playerResult : playerResults) {
            //si il y a eu changement, on prends la note du remplacant en compte (potentiellement rotaldo)
            if (playerResult.getSubstitute() != null) {
                playerRatings += playerResult.getSubstitute().getRating();
                if (playerResult.getSubstitute().getBonus() != null) {
                    playerRatings += playerResult.getSubstitute().getBonus();
                }
            } else {
                //sinon on prend la note du titulaire
                playerRatings += playerResult.getRating();
                if (playerResult.getBonus() != null) {
                    playerRatings += playerResult.getBonus();
                }
            }

        }

        if (lineType == 1) {
            log.info("playerRatings = " + playerRatings);
            log.info("playerResults.size() = " + playerResults.size());
            log.info("playerRatings / playerResults.size() = " + playerRatings / playerResults.size());
        }
        return playerRatings / playerResults.size();
    }

    /**
     * Retourne la liste des matchs de l'utilisateur sur une ligue donnée.
     * @param user L'utilisateur
     * @param results L'objet contenant la liste des matchs de la ligue
     * @return Retourne la liste des matchs d'un utilisateur sur une ligue donnée
     */
    private List<Match> getUserMatches(User user, Results results) {
        return results.getMatches()
                        .stream()
                        .filter(match -> match.getTeamHome().getUser().equals(user.getUserId())
                                || match.getTeamAway().getUser().equals(user.getUserId()))
                        .collect(Collectors.toList());
    }

    public void initLeagueResults(String leagueId, User user,  HttpSession session) {
        //on recupere le calendrier de la ligue pour avoir le nombre de matchs joués
        Results leagueDefaultResults = getLeagueCalendar(leagueId, null, user);
        Integer currentMatchDay = leagueDefaultResults.getCurrentMatchDay();
        session.setAttribute("league_" + leagueId + "_currentMatchDay", currentMatchDay);

        List<Results> leagueResults = new ArrayList<>();
        //on recupère l'ensemble des matchs joués
        for (int i = 1; i <= currentMatchDay; i++) {
            Results championshipDayResults = getLeagueCalendar(leagueId, i, user);
            if (championshipDayResults != null) {
                leagueResults.add(championshipDayResults);
            }
        }
        //ajout des resultats de la saison en session pour ne plus appeler MPG par la suite
        session.setAttribute("leagueResults_" + leagueId, leagueResults);

        //on filtre les matchs de l'utilisateur pour les mettre directment en session
        List<Match> userMatchs = new ArrayList<>();
        for (Results results: leagueResults) {
            userMatchs.addAll(getUserMatches(user, results));
        }
        session.setAttribute("userMatchs_league" + leagueId, userMatchs);

    }

    public List<TeamPlayer> getLeaguePlayers(String leagueId, User user, HttpSession session) {

        List<Results> results = (List<Results>) session.getAttribute("leagueResults_" + leagueId);
        if (results == null) {
            initLeagueResults(leagueId, user, session);
            results = (List<Results>) session.getAttribute("leagueResults_" + leagueId);
        }

        Map<String, TeamPlayer> teamPlayerMap = new HashMap<>();
        for (Match match : results.get(0).getMatches()) {
            TeamPlayer homeTeamPlayer = new TeamPlayer();
            homeTeamPlayer.setId(match.getTeamHome().getId());
            homeTeamPlayer.setName(match.getTeamHome().getName());
            homeTeamPlayer.setUserId(match.getTeamHome().getUser());

            TeamPlayer awayTeamPlayer = new TeamPlayer();
            awayTeamPlayer.setId(match.getTeamAway().getId());
            awayTeamPlayer.setName(match.getTeamAway().getName());
            awayTeamPlayer.setUserId(match.getTeamAway().getUser());

            if (!teamPlayerMap.containsKey(homeTeamPlayer.getName())) {
                teamPlayerMap.put(homeTeamPlayer.getName().toLowerCase(), homeTeamPlayer);
            }
            if (!teamPlayerMap.containsKey(awayTeamPlayer.getName())) {
                teamPlayerMap.put(awayTeamPlayer.getName().toLowerCase(), awayTeamPlayer);
            }
        }

        Map result = teamPlayerMap.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));

        return new ArrayList<TeamPlayer>(result.values());
    }

    public TeamRankingHistoryResponse getTeamRankingHistory(LinkedHashMap<Integer, LinkedHashMap<String, TeamRankingData>> leagueRankingHistory) {
        TeamRankingHistoryResponse response = new TeamRankingHistoryResponse();
        List<TeamRankingDataSet> teamRankingDataSets = new ArrayList<>();
        LinkedHashMap<String, TeamRankingData> dayRanking = leagueRankingHistory.get(1);
        //pour chaque equipe, on va recuperer leur position sur chaque journée
        for (String teamName : dayRanking.keySet()) {
            List<Integer> positionsTeam = new ArrayList<>();
            for (Integer day : leagueRankingHistory.keySet()) {
                LinkedHashMap<String, TeamRankingData> teamRankingDataMap = leagueRankingHistory.get(day);
                int i = 1;
                for (String dataTeamName : teamRankingDataMap.keySet()) {
                    if (dataTeamName.equals(teamName)) {
                        positionsTeam.add(Math.negateExact(i));
                        break;
                    }
                    i++;
                }
            }

            TeamRankingDataSet teamRankingDataSet = new TeamRankingDataSet();
            teamRankingDataSet.setData(positionsTeam);
            teamRankingDataSet.setLabel(teamName);
            teamRankingDataSets.add(teamRankingDataSet);
        }

        for (Integer day: leagueRankingHistory.keySet()) {
            response.getChartLabels().add(String.valueOf(day));
        }
        response.setDataSets(teamRankingDataSets);
        return response;
    }

    public List<Match> getLastGames(String leagueId, String userId, User user, HttpSession session) {

        List<Match> matches = (List<Match>) session.getAttribute("userMatchs_league" + leagueId);
        if (matches == null) {
            initLeagueResults(leagueId, user, session);
            matches = (List<Match>) session.getAttribute("userMatchs_league" + leagueId);
        }

        return matches;
    }

}
