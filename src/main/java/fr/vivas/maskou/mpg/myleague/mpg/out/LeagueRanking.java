package fr.vivas.maskou.mpg.myleague.mpg.out;

import java.util.List;

public class LeagueRanking {

    private List<RankingData> ranking;
    private List<RankingTeams> teams;

    public LeagueRanking() {
    }

    public List<RankingData> getRanking() {
        return ranking;
    }

    public void setRanking(List<RankingData> ranking) {
        this.ranking = ranking;
    }

    public List<RankingTeams> getTeams() {
        return teams;
    }

    public void setTeams(List<RankingTeams> teams) {
        this.teams = teams;
    }

    @Override
    public String toString() {
        return "{" +
                "ranking: " + ranking +
                ", teams: " + teams +
                '}';
    }
}
