package fr.vivas.maskou.mpg.myleague.services;

import fr.vivas.maskou.mpg.myleague.models.User;
import fr.vivas.maskou.mpg.myleague.mpg.out.LeaguesOut;
import fr.vivas.maskou.mpg.myleague.mpg.out.ResultMatchOut;
import fr.vivas.maskou.mpg.myleague.mpg.out.ResultMatchOutData;
import fr.vivas.maskou.mpg.myleague.utils.RestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Component
public class MatchService {
    private static final Logger log = LoggerFactory.getLogger(MatchService.class);
    private static final String URL_GET_MATCH = "https://api.monpetitgazon.com/league/<league_id>/results/<match_id>";

    public ResultMatchOutData getMatch(String matchId, String leagueId, User user) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = RestUtils.getHttpHeaders(user.getToken());
        HttpEntity<LeaguesOut> entity = new HttpEntity<LeaguesOut>(headers);
        String urlGetMatch = URL_GET_MATCH.replace("<league_id>", leagueId);
        urlGetMatch = urlGetMatch.replace("<match_id>", matchId);
        try {

            ResponseEntity<ResultMatchOut> response = restTemplate.exchange(urlGetMatch, HttpMethod.GET,
                    entity, ResultMatchOut.class);
            if (response.getStatusCode() == HttpStatus.OK) {
                log.info(response.getBody().toString());

                return response.getBody().getData();
            }
            return null;
        } catch (HttpClientErrorException e) {
            log.info("Impossible de récuperer les données du match " + leagueId + "/" + matchId);
            return null;
        }
    }
}
