package fr.vivas.maskou.mpg.myleague.controllers;

import fr.vivas.maskou.mpg.myleague.models.User;
import fr.vivas.maskou.mpg.myleague.models.UserIn;
import fr.vivas.maskou.mpg.myleague.mpg.out.UserInfos;
import fr.vivas.maskou.mpg.myleague.services.ConnectionService;
import fr.vivas.maskou.mpg.myleague.services.LeagueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@RestController
public class ConnectionController {
    private static final Logger log = LoggerFactory.getLogger(ConnectionController.class);

    @Autowired
    private ConnectionService connectionService;

    @CrossOrigin(origins="http://localhost:4200")
    @RequestMapping(value = "/connect",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public User connect(@RequestBody UserIn user,
                        HttpSession session) {
        User userOut = connectionService.connect(user);
        session.setAttribute("user", userOut);
        log.info(userOut.toString());
        return userOut;
    }

    @RequestMapping(value = "/connect",
            method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public User connectGet(@RequestParam(name = "l") String login,
                           @RequestParam(name = "p") String password,
                           HttpSession session) {
        UserIn user = new UserIn();
        user.setLogin(login);
        user.setPassword(password);
        User userOut = connectionService.connect(user);
        session.setAttribute("user", user);
        return userOut;
    }

    @CrossOrigin(origins="http://localhost:4200")
    @RequestMapping(value = "/user/infos",
            method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UserInfos getUserInfos(@RequestBody User user,
                                  HttpSession session) {
        return connectionService.getUserInfo(user);
    }

}
