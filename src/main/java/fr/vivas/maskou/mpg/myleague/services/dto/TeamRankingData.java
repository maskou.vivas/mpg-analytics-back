package fr.vivas.maskou.mpg.myleague.services.dto;

public class TeamRankingData {
    public double points = 0;
    public double goalAverage = 0;

    public TeamRankingData() {
    }

    public double getPoints() {
        return points;
    }

    public void setPoints(double points) {
        this.points = points;
    }

    public double getGoalAverage() {
        return goalAverage;
    }

    public void setGoalAverage(double goalAverage) {
        this.goalAverage = goalAverage;
    }

    @Override
    public String toString() {
        return "{" +
                "points: " + points +
                ", goalAverage: " + goalAverage +
                '}';
    }

}
