package fr.vivas.maskou.mpg.myleague;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class MpgAnalyticsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MpgAnalyticsApplication.class, args);
	}

}
