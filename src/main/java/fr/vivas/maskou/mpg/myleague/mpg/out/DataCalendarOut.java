package fr.vivas.maskou.mpg.myleague.mpg.out;

public class DataCalendarOut {
    private Results results;

    public DataCalendarOut() {
    }

    public Results getResults() {
        return results;
    }

    public void setResults(Results results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return "{" +
                "results: " + results +
                '}';
    }
}
