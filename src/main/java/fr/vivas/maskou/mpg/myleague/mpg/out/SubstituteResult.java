package fr.vivas.maskou.mpg.myleague.mpg.out;

public class SubstituteResult {
    private Integer number;
    private Integer rating;
    private String starterName;
    private String substituteName;

    public SubstituteResult() {
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getStarterName() {
        return starterName;
    }

    public void setStarterName(String starterName) {
        this.starterName = starterName;
    }

    public String getSubstituteName() {
        return substituteName;
    }

    public void setSubstituteName(String substituteName) {
        this.substituteName = substituteName;
    }

    @Override
    public String toString() {
        return "{" +
                "number: " + number +
                ", rating: " + rating +
                ", starterName: '" + starterName + '\'' +
                ", substituteName: '" + substituteName + '\'' +
                '}';
    }
}
