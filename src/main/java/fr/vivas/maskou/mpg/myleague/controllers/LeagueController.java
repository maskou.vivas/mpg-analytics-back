package fr.vivas.maskou.mpg.myleague.controllers;
import fr.vivas.maskou.mpg.myleague.models.*;
import fr.vivas.maskou.mpg.myleague.mpg.out.Match;
import fr.vivas.maskou.mpg.myleague.mpg.out.Results;
import fr.vivas.maskou.mpg.myleague.services.LeagueRankingService;
import fr.vivas.maskou.mpg.myleague.services.LeagueService;
import fr.vivas.maskou.mpg.myleague.services.dto.LeagueWithPlayers;
import fr.vivas.maskou.mpg.myleague.services.dto.TeamRankingData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

@RestController
@RequestMapping(path = "/league")
public class LeagueController {

    @Autowired
    private LeagueRankingService leagueReankingService;

    @Autowired
    private LeagueService leagueService;

    @CrossOrigin(origins="http://localhost:4200")
    @RequestMapping(value = "/all", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<LeagueWithPlayers> list(@RequestBody User user,
                                        HttpSession session) {
        return leagueService.getLeagues(user, session);
    }

    @RequestMapping(value = "/{leagueid}/calendar/{championshipDay}",
            method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Results getLeagueCalendar(@PathVariable("leagueid") String ligueId,
                                     @PathVariable("championshipDay") Integer championshipDay,
                                     HttpSession session) {
        User user = (User) session.getAttribute("user");
        return leagueService.getLeagueCalendar(ligueId, championshipDay, user);
    }

    @CrossOrigin(origins="http://localhost:4200")
    @RequestMapping(value = "/{leagueid}",
            method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public IndexData index(@PathVariable("leagueid") String ligueId,
                           @RequestBody User user,
                           HttpSession session) {
        leagueService.initLeagueResults(ligueId, user, session);
        IndexData playerData = leagueService.getPlayerData(ligueId, user, session);
        return playerData;
    }


    @CrossOrigin(origins="http://localhost:4200")
    @RequestMapping(value = "/{leagueid}/user/{userid}",
            method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public IndexData statsForPlayer(@PathVariable("leagueid") String ligueId,
                                    @PathVariable("userid") String userId,
                                    @RequestBody User user,
                                    HttpSession session) {
        user.setUserId(userId);
        leagueService.initLeagueResults(ligueId, user, session);
        return leagueService.getPlayerData(ligueId, user,  session);
    }

    @CrossOrigin(origins="http://localhost:4200")
    @RequestMapping(value = "/{leagueid}/ranking/history",
            method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public TeamRankingHistoryResponse rankingHistory(
                                    @PathVariable("leagueid") String ligueId,
                                    @RequestBody User user,
                                    HttpSession session) {
        leagueService.initLeagueResults(ligueId, user, session);
        LinkedHashMap<Integer, LinkedHashMap<String, TeamRankingData>> leagueRankingHistory =
                leagueReankingService.getLeagueRankingHistory(ligueId, session);

        return leagueService.getTeamRankingHistory(leagueRankingHistory);
    }

    @CrossOrigin(origins="http://localhost:4200")
    @RequestMapping(value = "/{leagueid}/user/{userid}/games", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Match> getUserLastGames(@PathVariable("leagueid") String ligueId,
                                        @PathVariable("userid") String userId,
                                        @RequestBody User user,
                                        HttpSession session) {
        user.setUserId(userId);
        return leagueService.getLastGames(ligueId, userId, user, session);
    }
}
