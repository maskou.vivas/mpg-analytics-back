package fr.vivas.maskou.mpg.myleague.mpg.out;

public class PlayerScorer {
    private String id;
    private String player;
    private Integer real;
    private Integer mpg;
    private Integer goals;
    private String teamid;
    private String realTeamId;

    public PlayerScorer() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public Integer getReal() {
        return real;
    }

    public void setReal(Integer real) {
        this.real = real;
    }

    public Integer getMpg() {
        return mpg;
    }

    public void setMpg(Integer mpg) {
        this.mpg = mpg;
    }

    public Integer getGoals() {
        return goals;
    }

    public void setGoals(Integer goals) {
        this.goals = goals;
    }

    public String getTeamid() {
        return teamid;
    }

    public void setTeamid(String teamid) {
        this.teamid = teamid;
    }

    public String getRealTeamId() {
        return realTeamId;
    }

    public void setRealTeamId(String realTeamId) {
        this.realTeamId = realTeamId;
    }

    @Override
    public String toString() {
        return "{" +
                "id:'" + id + '\'' +
                ", player:'" + player + '\'' +
                ", real:" + real +
                ", mpg:" + mpg +
                ", goals:" + goals +
                ", teamid:'" + teamid + '\'' +
                ", realTeamId:'" + realTeamId + '\'' +
                '}';
    }
}
