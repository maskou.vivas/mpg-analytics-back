package fr.vivas.maskou.mpg.myleague.mpg.out;

public class MpgConnectionIn {
    private String email;
    private String password;
    private String language;

    public MpgConnectionIn() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public String toString() {
        return "{" +
                "email: '" + email + '\'' +
                ", password: '" + password + '\'' +
                ", language: '" + language + '\'' +
                '}';
    }
}
