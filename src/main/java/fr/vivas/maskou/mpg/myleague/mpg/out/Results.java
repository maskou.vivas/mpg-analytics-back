package fr.vivas.maskou.mpg.myleague.mpg.out;

import java.util.List;

public class Results {
    private Integer currentMatchDay;
    private List<Match> matches;
    private Integer maxMatchDay;

    public Results() {
    }

    public Integer getCurrentMatchDay() {
        return currentMatchDay;
    }

    public void setCurrentMatchDay(Integer currentMatchDay) {
        this.currentMatchDay = currentMatchDay;
    }

    public List<Match> getMatches() {
        return matches;
    }

    public void setMatches(List<Match> matches) {
        this.matches = matches;
    }

    public Integer getMaxMatchDay() {
        return maxMatchDay;
    }

    public void setMaxMatchDay(Integer maxMatchDay) {
        this.maxMatchDay = maxMatchDay;
    }

    @Override
    public String toString() {
        return "{" +
                "currentMatchDay: " + currentMatchDay +
                ", matches: " + matches +
                ", maxMatchDay: " + maxMatchDay +
                '}';
    }
}
